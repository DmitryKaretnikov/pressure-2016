package com.jackalopeapps.gameworld;

import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.jackalopeapps.gameobjects.Brick;
import com.jackalopeapps.gameobjects.Cell;

public class GameColListener implements ContactListener {
    private GameStage stage;
    private int colMode;

    public GameColListener(GameStage stage, int c) {
        this.stage = stage;
        colMode = c;
    }

    @Override
    public void beginContact(Contact contact) {
    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();
        if (fixtureA.getShape() instanceof CircleShape && fixtureB.getShape() instanceof CircleShape) {
            Cell a = (Cell) fixtureA.getBody().getUserData();
            Cell b = (Cell) fixtureB.getBody().getUserData();
            if (a.getSize() == b.getSize() && !a.deleteFlag && !b.deleteFlag) {
                if (colMode != 7) {
                    collide(a, b, 0);
                } else {
                    collideColor(a, b);
                }
            }
        }
        if (fixtureA.getShape() instanceof PolygonShape && fixtureB.getShape() instanceof CircleShape) {
            Cell a = (Cell) fixtureB.getBody().getUserData();
            a.abc();
        }
        if (fixtureA.getShape() instanceof CircleShape && fixtureB.getShape() instanceof PolygonShape) {
            Cell a = (Cell) fixtureA.getBody().getUserData();
            a.abc();
        }
    }

    private void collideColor(Cell a, Cell b) {
        if (a.getCl() == b.getCl() || b.getCl() == 6) {
            collide(a, b, a.getCl());
            return;
        }
        if (a.getCl() == 6) {
            collide(a, b, b.getCl());
            return;
        }

        if ((a.getCl() == 0 || b.getCl() == 0) && (a.getCl() == 2 || b.getCl() == 2)) {
            collide(a, b, 1);
            return;
        }
        if ((a.getCl() == 2 || b.getCl() == 2) && (a.getCl() == 4 || b.getCl() == 4)) {
            collide(a, b, 3);
            return;
        }
        if ((a.getCl() == 4 || b.getCl() == 4) && (a.getCl() == 0 || b.getCl() == 0)) {
            collide(a, b, 5);
            return;
        }

        if ((a.getCl() == 1 || b.getCl() == 1) && (a.getCl() == 3 || b.getCl() == 3)) {
            collide(a, b, 2);
            return;
        }
        if ((a.getCl() == 3 || b.getCl() == 3) && (a.getCl() == 5 || b.getCl() == 5)) {
            collide(a, b, 4);
            return;
        }
        if ((a.getCl() == 5 || b.getCl() == 5) && (a.getCl() == 1 || b.getCl() == 1)) {
            collide(a, b, 0);
            return;
        }
    }

    private void collide(Cell a, Cell b, int col) {
        stage.createCell(b.getBody().getPosition().x + (a.getBody().getPosition().x - b.getBody().getPosition().x) / 2,
                b.getBody().getPosition().y + (a.getBody().getPosition().y - b.getBody().getPosition().y) / 2,
                a.getSize() * 1.5f, col, a.getPrice() * 3);
        b.deleteFlag = !b.deleteFlag;
        a.deleteFlag = !a.deleteFlag;
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
}
