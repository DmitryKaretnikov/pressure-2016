package com.jackalopeapps.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.jackalopeapps.gameobjects.Brick;
import com.jackalopeapps.gameobjects.Cell;
import com.jackalopeapps.pressure.Prefs;

import java.util.ArrayList;

public class GameRenderer extends Box2DDebugRenderer {
    private BitmapFont font, shadow;
    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch batch;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private GameStage stage;
    private ArrayList<Cell> cells;
    private ArrayList<Brick> bricks;
    private Prefs pref;

    public GameRenderer(GameStage s, Prefs p, OrthographicCamera c) {
        camera = c;
        pref = p;

        generator = new FreeTypeFontGenerator(Gdx.files.internal("text.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Gdx.graphics.getWidth()/36;
        parameter.color = Color.BLACK;
        font = generator.generateFont(parameter);
        parameter.color = Color.GRAY;
        shadow = generator.generateFont(parameter);
        generator.dispose();
        batch = new SpriteBatch();

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);

        stage = s;
        cells = stage.getCells();

        bricks = stage.getBricks();
    }

    public void rend(int score) {
        Gdx.gl.glClearColor(0.7f, 0.7f, 0.7f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (Brick b : bricks) {
            shapeRenderer.setColor(0.3f, 0.3f, 0.3f, 0);
            shapeRenderer.rect(b.getBody().getPosition().x - b.getWidth(),
                    b.getBody().getPosition().y - (b).getHeight(),
                    (b).getWidth() * 2, (b).getHeight() * 2);
        }

        for (Cell c : cells) {
            shapeRenderer.setColor(Color.BLACK);
            shapeRenderer.circle(c.getBody().getPosition().x, c.getBody().getPosition().y,
                    (c.getSize() * Gdx.graphics.getWidth() / 72) + 2, 40);

            if (pref.getColMode() == 7)
                setCol(c.getCl(), c);
            else
                setCol(pref.getColMode(), c);

            shapeRenderer.circle(c.getBody().getPosition().x, c.getBody().getPosition().y,
                    (c.getSize() * Gdx.graphics.getWidth() / 72), 40);

//            shapeRenderer.setColor(Color.BLACK);
//            shapeRenderer.circle(c.getBody().getPosition().x, c.getBody().getPosition().y, 4, 40);
        }
        shapeRenderer.end();

        batch.begin();
        shadow.draw(batch, "|Time| " + (pref.getTime() - stage.getTime()) + "|" + pref.getTime() + " sec", Gdx.graphics.getWidth() / 18, Gdx.graphics.getWidth()/18);
        font.draw(batch, "|Time| " + (pref.getTime() - stage.getTime()) + "|" + pref.getTime() + " sec", Gdx.graphics.getWidth() / 18 - 2, Gdx.graphics.getWidth()/18 + 2);
        shadow.draw(batch, "|Pressure| " + pref.getStringScore(score), Gdx.graphics.getWidth() / 18, Gdx.graphics.getWidth()/36);
        font.draw(batch, "|Pressure| " + pref.getStringScore(score), Gdx.graphics.getWidth() / 18 - 2, Gdx.graphics.getWidth()/36 + 2);
        batch.end();
    }

    private void setCol(int cl, Cell c) {
        switch (cl) {
            case 0:
                shapeRenderer.setColor((c.getSize() * 4 + 96) / 255f, 40 / 255f,
                        40 / 255f, 0);
                break;
            case 1:
                shapeRenderer.setColor((c.getSize() * 4 + 96) / 255f, (c.getSize() * 4 + 96) / 255f,
                        40 / 255f, 0);
                break;
            case 2:
                shapeRenderer.setColor(40 / 255f, (c.getSize() * 4 + 96) / 255f,
                        40 / 255f, 0);
                break;
            case 3:
                shapeRenderer.setColor(40 / 255f, (c.getSize() * 4 + 96) / 255f,
                        (c.getSize() * 4 + 96) / 255f, 0);
                break;
            case 4:
                shapeRenderer.setColor(40 / 255f, 40 / 255f,
                        (c.getSize() * 4 + 96) / 255f, 0);
                break;
            case 5:
                shapeRenderer.setColor((c.getSize() * 4 + 96) / 255f, 40 / 255f,
                        (c.getSize() * 4 + 96) / 255f, 0);

                break;
            default:
                shapeRenderer.setColor((c.getSize() * 4 + 96) / 255f, (c.getSize() * 4 + 96) / 255f,
                        (c.getSize() * 4 + 96) / 255f, 0);
                break;
        }
    }
}