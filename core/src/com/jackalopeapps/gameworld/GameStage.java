package com.jackalopeapps.gameworld;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jackalopeapps.gameobjects.Brick;
import com.jackalopeapps.gameobjects.Cell;
import com.jackalopeapps.pressure.Prefs;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameStage extends Stage {
    private boolean tWork = true;
    private int createСounter, createSpeed = 20, wallСounter, time, score;
    private float accumulator = 0f;
    private GameRenderer renderer;
    private World world;
    private ArrayList<Cell> cells, deleteCArray;
    private ArrayList<GameCreator> cre;
    private ArrayList<Brick> bricks, deleteBArray;
    private Random rand;
    private Timer timer;
    private Prefs pref;

    public GameStage(Prefs p, OrthographicCamera c) {
        pref = p;
        deleteCArray = new ArrayList<Cell>();
        deleteBArray = new ArrayList<Brick>();
        cells = new ArrayList<Cell>();
        cre = new ArrayList<GameCreator>();
        bricks = new ArrayList<Brick>();
        rand = new Random();
        world = new World(new Vector2(0, 0), true);
        world.setContactListener(new GameColListener(this, pref.getColMode()));
        time = 0;
        timer = new Timer();
        timer.schedule(new Counter(), 0, 1000);
        renderer = new GameRenderer(this, pref, c);
        while (wallСounter < Gdx.graphics.getWidth() / 36) {
            createBrick();
            wallСounter++;
        }
    }

    @Override
    public void act(float delta) {
        if (pref.getG()) {
            accumulator += delta;
            while (accumulator >= delta) {
                world.step(1 / 180f, 1, 1);
                accumulator -= 1 / 180f;
            }

            for (Cell c : cells) {
                c.update();
                if (c.getBody().getPosition().x > Gdx.graphics.getWidth() + c.getSize() * Gdx.graphics.getWidth() / 72 ||
                        c.getBody().getPosition().y > Gdx.graphics.getHeight() + c.getSize() * Gdx.graphics.getWidth() / 72 ||
                        c.getBody().getPosition().x < 0 - c.getSize() * Gdx.graphics.getWidth() / 72 ||
                        c.getBody().getPosition().y < 0 - c.getSize() * Gdx.graphics.getWidth() / 72) {
                    c.deleteFlag = true;
                }
            }

            for (Brick b : bricks) {
                if (b.getBody().getPosition().x > Gdx.graphics.getWidth() + 20 ||
                        b.getBody().getPosition().y > Gdx.graphics.getHeight() + 20 ||
                        b.getBody().getPosition().x < -20 || b.getBody().getPosition().y < -20) {
                    b.deleteFlag = true;
                }
                b.getBody().setLinearVelocity(0, 0);
            }

            for (Cell c : cells)
                if (c.deleteFlag) {
                    deleteCArray.add(c);
                }
            for (Cell c : deleteCArray) {
                if (cells.contains(c)) {
                    cells.remove(c);
                }
                world.destroyBody(c.getBody());
                c.getBody().setUserData(null);
                c.remove();
            }
            deleteCArray.clear();

            for (Brick b : bricks)
                if (b.deleteFlag) {
                    deleteBArray.add(b);
                }
            for (Brick b : deleteBArray) {
                if (bricks.contains(b)) {
                    bricks.remove(b);
                }
                world.destroyBody(b.getBody());
                b.getBody().setUserData(null);
                b.remove();
                createBrick();
            }
            deleteBArray.clear();

            try {
                if (!cre.isEmpty()) {
                    for (GameCreator cr : cre) {
                        cells.add(new Cell(cr.getX(), cr.getY(), cr.getS(), cr.getCl(), cr.getPr(), world));
                    }
                    cre.clear();
                }
                if (createСounter == createSpeed) {
                    createCell();
                    createСounter = 0;
                } else {
                    createСounter++;
                }
            } catch (Exception ex) {
            }

            score = 0;
            for (Cell c : cells)
                if (c.getPrice() > 3)
                    score += c.getPrice();
            pref.setPreScore(score);

            if (time == pref.getTime()) {
                stopgame();
            }
            tWork = true;
            super.act(delta);
        }
    }

    public void stopgame() {
        if (score > pref.getHighScore())
            pref.setHighScore(score);
        pref.getGame().setM();
    }

    @Override
    public void draw() {
        super.draw();
        renderer.rend(score);
    }

    public void dispose() {
        renderer.dispose();
    }

    private void createCell() {
        int s = rand.nextInt(2) + 1;
        int cl = rand.nextInt(7);
        float x = rand.nextInt(Gdx.graphics.getWidth() - 60) + 30;
        float y = rand.nextInt(Gdx.graphics.getHeight() - 60) + 30;
        switch (s) {
            case 1:
                cells.add(new Cell(x, y, 1, cl, 1, world));
                break;
            case 2:
                cells.add(new Cell(x, y, 1.5f, cl, 3, world));
                break;
        }
    }

    public void createCell(float x, float y, float s, int cl, int pr) {
        try {
            cre.add(new GameCreator(x, y, s, cl, pr));
        } catch (Exception ex) {
        }
    }

    private void createBrick() {
        int s = rand.nextInt(3) + 1;
        float y = rand.nextInt(Gdx.graphics.getHeight() - 150) + 100;
        float x = rand.nextInt(Gdx.graphics.getWidth() - 100) + 50;
        bricks.add(new Brick(x, y, s, world));
    }

    public void onSwipe(Vector2 acceleration) {
        for (Cell c : cells)
            c.setVelocity(acceleration);
    }

    public ArrayList<Cell> getCells() {
        return cells;
    }

    public ArrayList<Brick> getBricks() {
        return bricks;
    }

    public int getTime() {
        return time;
    }

    class Counter extends TimerTask {
        public void run() {
            if (pref.getG() && tWork) {
                time++;
                if (pref.getTime() - time == 10 || pref.getTime() - time == 5) {
                    Gdx.input.vibrate(500);
                }
                tWork = false;
            }
        }
    }
}