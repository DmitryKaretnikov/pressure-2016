package com.jackalopeapps.gameworld;

public class GameCreator {
    private float x, y, s;
    private int cl, pr;

    public GameCreator(float x, float y, float s, int cl, int pr) {
        this.x = x;
        this.y = y;
        this.s = s;
        this.cl = cl;
        this.pr = pr;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getS() {
        return s;
    }

    public int getPr() {
        return pr;
    }

    public int getCl() {
        return cl;
    }
}
