package com.jackalopeapps.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Brick extends Actor {
    public boolean deleteFlag;
    private BodyDef bodyDef = new BodyDef();
    private Body body;
    private float width;
    private float height;

    public Brick(float x, float y, int s, World wr) {
        width = Gdx.graphics.getWidth() / 36 - (s * Gdx.graphics.getWidth() / 144);
        height = s * Gdx.graphics.getWidth() / 144;
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        makeBrick(x, y, wr);
    }

    public Brick(float x, float y, int w, int h, World wr) {
        width = w;
        height = h;
        bodyDef.type = BodyDef.BodyType.StaticBody;
        makeBrick(x, y, wr);
    }

    public void makeBrick(float x, float y, World wr) {
        bodyDef.position.set(new Vector2(x, y));
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height);
        body = wr.createBody(bodyDef);
        body.createFixture(shape, 0);
        shape.dispose();
    }

    public Body getBody() {
        return body;
    }

    @Override
    public float getHeight() {
        return height;
    }

    @Override
    public float getWidth() {
        return width;
    }
}
