package com.jackalopeapps.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class Cell extends Button {
    public boolean deleteFlag;
    private BodyDef bodyDef;
    private Body body;
    private int color, price;
    private float distance, size;
    private Vector2 pre, p, fin, acceleration;
    private CircleShape shape;

    public Cell(float x, float y, float s, int cl, int pr, World wr) {
        size = s;
        color = cl;
        price = pr;
        fin = new Vector2(0, 0);
        bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(x, y));
        shape = new CircleShape();
        shape.setRadius(this.size * Gdx.graphics.getWidth() / 72);
        body = wr.createBody(bodyDef);
        body.createFixture(shape, 0.0001f);
        body.setUserData(this);
        pre = body.getPosition();
        shape.dispose();
    }

    public void update() {
        p = body.getPosition().cpy();
        if (distance < 0) {
            body.setLinearVelocity(0, 0);
            distance = 0;
            fin.x = 0;
            fin.y = 0;
        } else if (distance > 0) {
            distance = distance - pre.dst(p);
        }
        pre = p;
    }

    public void setVelocity(Vector2 a) {
        if (fin.x == 0 && fin.y == 0) {
            fin = body.getPosition().cpy();
            fin.add(a.cpy());
        } else {
            fin.add(a.cpy());
        }
        acceleration = fin.cpy();
        acceleration.sub(body.getPosition().cpy());
        distance = acceleration.cpy().len();
        body.applyLinearImpulse(acceleration, body.getPosition(), true);
    }


    public int getPrice() {
        return price;
    }

    public float getSize() {
        return size;
    }

    public void abc() {
        acceleration = fin.cpy();
        acceleration.sub(body.getPosition().cpy());
        body.applyLinearImpulse(acceleration, body.getPosition(), true);
        body.setLinearVelocity(body.getLinearVelocity().cpy().scl(0.75f));
    }

    public int getCl() {
        return color;
    }

    public Body getBody() {
        return body;
    }
}