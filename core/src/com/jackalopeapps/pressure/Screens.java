package com.jackalopeapps.pressure;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jackalopeapps.gameworld.GameInput;
import com.jackalopeapps.gameworld.GameStage;
import com.jackalopeapps.menuworld.MenuInput;
import com.jackalopeapps.menuworld.MenuStage;

public class Screens implements Screen {
    private Stage stage;
    private OrthographicCamera camera;

    public Screens(Prefs p, boolean m) {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0f);
        camera.update();

        if (m) {
            stage = new MenuStage(p, camera);
            Gdx.input.setInputProcessor(new MenuInput((MenuStage) stage, p, camera));
        } else {
            stage = new GameStage(p, camera);
            Gdx.input.setInputProcessor(new GameInput((GameStage) stage));
        }
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
