package com.jackalopeapps.pressure;

import com.badlogic.gdx.Game;

public class PressureGame extends Game  {
	private Screens screen;
	private Prefs p;

	@Override
	public void create() {
		p = new Prefs(this);
		setM();
		p.setG();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	public void setM(){
		p.setG();
		screen = new Screens(p, true);
		setScreen(screen);
	}

	public void setG(){
		p.setG();
		screen = new Screens(p, false);
		setScreen(screen);
	}
}