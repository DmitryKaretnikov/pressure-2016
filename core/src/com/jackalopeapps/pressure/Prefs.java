package com.jackalopeapps.pressure;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class Prefs {
    private static Preferences prefs;
    private boolean G = false;
    private final PressureGame game;

    public Prefs(PressureGame g) {
        game = g;
        prefs = Gdx.app.getPreferences("test4");
        if (!prefs.contains("time")) {
            prefs.putInteger("time", 30);
        }
        if (!prefs.contains("colMode")) {
            prefs.putInteger("colMode", 0);
        }
        if (!prefs.contains("hardPhysic")) {
            prefs.putBoolean("hardPhysic", true);
        }
        if (!prefs.contains("highScore" + prefs.getInteger("time"))) {
            prefs.putInteger("highScore" + prefs.getInteger("time"), 0);
        }
        if (!prefs.contains("highScore+" + prefs.getInteger("time"))) {
            prefs.putInteger("highScore+" + prefs.getInteger("time"), 0);
        }
        if (!prefs.contains("previousScore" + prefs.getInteger("time"))) {
            prefs.putInteger("previousScore" + prefs.getInteger("time"), 0);
        }
        if (!prefs.contains("previousScore+" + prefs.getInteger("time"))) {
            prefs.putInteger("previousScore+" + prefs.getInteger("time"), 0);
        }
        prefs.flush();
    }

    public boolean getHardPhysic() {
        return prefs.getBoolean("hardPhysic");
    }

    public void setHardPhysic() {
        prefs.putBoolean("hardPhysic", !getHardPhysic());
        prefs.flush();
    }

    public int getColMode() {
        return prefs.getInteger("colMode");
    }

    public void setColMode() {
        int t = getColMode();
        t++;
        if (t == 8)
            t = 0;
        prefs.putInteger("colMode", t);
        prefs.flush();
    }

    public int getTime() {
        return prefs.getInteger("time");
    }

    public void setTime() {
        int t = getTime();
        switch (t){
            case 30: t = 60; break;
            case 60: t = 90; break;
            case 90: t = 120; break;
            case 120: t = 300; break;
            case 300: t = 600; break;
            case 600: t = 30; break;
        }
        prefs.putInteger("time", t);
        prefs.flush();
    }

    public void setHighScore(int val) {
        if (getColMode()==7)
            prefs.putInteger("highScore+" + prefs.getInteger("time"), val);
        else
            prefs.putInteger("highScore" + prefs.getInteger("time"), val);
        prefs.flush();
    }

    public int getHighScore() {
        if (getColMode()==7)
            return prefs.getInteger("highScore+" + prefs.getInteger("time"));
        else
            return prefs.getInteger("highScore" + prefs.getInteger("time"));
    }

    public void setPreScore(int val) {
        if (getColMode()==7)
            prefs.putInteger("previousScore+" + prefs.getInteger("time"), val);
        else
            prefs.putInteger("previousScore" + prefs.getInteger("time"), val);

        prefs.flush();
    }

    public int getPreScore() {
        if (getColMode()==7)
            return prefs.getInteger("previousScore+" + prefs.getInteger("time"));
        else
            return prefs.getInteger("previousScore" + prefs.getInteger("time"));
    }

    public String getStringScore(int score) {
        if (score < getHighScore())
            return score + "|" + getHighScore();
        else return "" + score;
    }

    public String getMenuScore() {
        return getPreScore() + "|" + getHighScore();
    }

    public PressureGame getGame() {
        return game;
    }

    public void setG(){
        G=!G;
    }

    public boolean getG(){
        return G;
    }
}
