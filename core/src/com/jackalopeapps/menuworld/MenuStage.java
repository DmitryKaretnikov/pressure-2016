package com.jackalopeapps.menuworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jackalopeapps.gameobjects.Brick;
import com.jackalopeapps.gameobjects.Cell;
import com.jackalopeapps.pressure.Prefs;

import java.util.ArrayList;
import java.util.Random;

public class MenuStage extends Stage{
    private MenuRenderer renderer;
    private World world;
    private ArrayList<Cell> cells;
    private ArrayList<Brick> bricks;
    private float accumulator = 0f;
    private Random rand;

    public MenuStage(Prefs p, OrthographicCamera c) {
        rand = new Random();
        cells = new ArrayList<Cell>();
        bricks = new ArrayList<Brick>();
        world = new World(new Vector2(0, 0), true);
        renderer = new MenuRenderer(this, p, c);
        cells.add(new Cell(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 3.55f,
                24, rand.nextInt(7), 0, world));
        cells.add(new Cell(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 1.6f,
                9, rand.nextInt(7), 1, world));
        cells.add(new Cell(Gdx.graphics.getWidth() / 1.33f, Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 1.6f,
                9, rand.nextInt(7), 2, world));
        bricks.add(new Brick(0, Gdx.graphics.getHeight()-10, Gdx.graphics.getWidth(), 0, world));
        bricks.add(new Brick(0, 0, Gdx.graphics.getWidth(), 0, world));
        bricks.add(new Brick(Gdx.graphics.getWidth(), 0, 0, Gdx.graphics.getHeight(), world));
        bricks.add(new Brick(10, 0, 0, Gdx.graphics.getHeight(), world));
    }

    @Override
    public void act(float delta) {
        accumulator += delta;
        while (accumulator >= delta) {
            world.step(1/180f, 1, 1);
            accumulator -= 1/180f;
        }
        for (Cell c : cells) {
            c.update();
        }
        super.act(delta);
    }

    @Override
    public void draw() {
        super.draw();
        renderer.rend();
    }

    public void onSwipe(Vector2 acceleration) {
        for (Cell c : cells)
            c.setVelocity(acceleration);
    }

    public ArrayList<Cell> getCells() {
        return cells;
    }

    public World getWorld() {
        return world;
    }
}
