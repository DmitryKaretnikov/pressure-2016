package com.jackalopeapps.menuworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.jackalopeapps.gameobjects.Cell;
import com.jackalopeapps.pressure.Prefs;

public class MenuInput implements InputProcessor {
    private static int downX;
    private static int downY;
    private Prefs pref;
    private MenuStage stage;
    private Cell downCell, upCell;
    private QueryCallback callbackUp, callbackDown;
    private Vector3 testPoint;
    private OrthographicCamera camera;

    public MenuInput(MenuStage s, Prefs p, OrthographicCamera c) {
        stage = s;
        pref = p;
        camera = c;
        testPoint = new Vector3();
        callbackDown = new QueryCallback() {
            @Override
            public boolean reportFixture(Fixture fixture) {
                if (fixture.testPoint(testPoint.x, testPoint.y)) {
                    downCell = (Cell) fixture.getBody().getUserData();
                    return false;
                } else
                    return true;
            }
        };
        callbackUp = new QueryCallback() {
            @Override
            public boolean reportFixture(Fixture fixture) {
                if (fixture.testPoint(testPoint.x, testPoint.y)) {
                    upCell = (Cell) fixture.getBody().getUserData();
                    if(downCell==upCell)
                    switch (upCell.getPrice()) {
                        case 0:
                            pref.getGame().setG();
                            break;
                        case 1:
                            pref.setTime();
                            break;
                        case 2:
                            pref.setColMode();
                            break;
                    }
                    return false;
                } else
                    return true;
            }
        };
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            Gdx.app.exit();
            return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        testPoint.set(screenX, screenY, 0);
        camera.unproject(testPoint);
        stage.getWorld().QueryAABB(callbackDown, testPoint.x - 0.1f, testPoint.y - 0.1f, testPoint.x + 0.1f, testPoint.y + 0.1f);
        if (pointer==0) {
            downX = screenX;
            downY = screenY;
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        testPoint.set(screenX, screenY, 0);
        camera.unproject(testPoint);
        stage.getWorld().QueryAABB(callbackUp, testPoint.x - 0.1f, testPoint.y - 0.1f, testPoint.x + 0.1f, testPoint.y + 0.1f);
        if (pointer==0) {
            stage.onSwipe(new Vector2(screenX - downX, -(screenY - downY)));
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
