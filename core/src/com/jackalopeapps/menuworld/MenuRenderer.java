package com.jackalopeapps.menuworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.jackalopeapps.gameobjects.Cell;
import com.jackalopeapps.pressure.Prefs;

import java.util.ArrayList;

public class MenuRenderer extends Box2DDebugRenderer {
    private BitmapFont font, shadow;
    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch batch;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private MenuStage stage;
    private ArrayList<Cell> cells;
    private Prefs pref;
    private Texture tTexture, pTexture, cTexture;
    private Sprite tSprite, pSprite, cSprite;

    public MenuRenderer(MenuStage s, Prefs p, OrthographicCamera c) {
        pref = p;
        camera = c;

        generator = new FreeTypeFontGenerator(Gdx.files.internal("text.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Gdx.graphics.getWidth()/36;
        parameter.color = Color.BLACK;
        font = generator.generateFont(parameter);
        parameter.color = Color.GRAY;
        shadow = generator.generateFont(parameter);
        generator.dispose();
        batch = new SpriteBatch();

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);

        stage = s;
        cells = stage.getCells();

        pTexture = new Texture(Gdx.files.internal("play-button.png"));
        pTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        pSprite = new Sprite(pTexture, 0, 0, 256, 256);
        tTexture = new Texture(Gdx.files.internal("schedule-button.png"));
        tTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        tSprite = new Sprite(tTexture, 0, 0, 256, 256);
        cTexture = new Texture(Gdx.files.internal("painter-palette.png"));
        cTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        cSprite = new Sprite(cTexture, 0, 0, 256, 256);
    }

    public void rend() {
        Gdx.gl.glClearColor(0.7f, 0.7f, 0.7f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (Cell c : cells) {
            if (pref.getColMode() == 7)
                setCol(c.getCl(), c);
            else
                setCol(pref.getColMode(), c);

            shapeRenderer.circle(c.getBody().getPosition().x, c.getBody().getPosition().y,
                    (c.getSize() * Gdx.graphics.getWidth() / 72), 400);
        }
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        Gdx.gl.glLineWidth(60);
        shapeRenderer.setColor(Color.BLACK);
        for (Cell c : cells) {
            shapeRenderer.circle(c.getBody().getPosition().x - 10, c.getBody().getPosition().y + 10,
                    (c.getSize() * Gdx.graphics.getWidth() / 72), 400);
        }
        shapeRenderer.end();

        batch.begin();
        batch.draw(pSprite, cells.get(0).getBody().getPosition().x - Gdx.graphics.getWidth() / 4 + Gdx.graphics.getWidth() / 27,
                cells.get(0).getBody().getPosition().y + 10 - Gdx.graphics.getWidth() / 4,
                Gdx.graphics.getWidth() / 2, Gdx.graphics.getWidth() / 2);
        batch.draw(tSprite, cells.get(1).getBody().getPosition().x - 10 - Gdx.graphics.getWidth() / 14,
                cells.get(1).getBody().getPosition().y + 10 - Gdx.graphics.getWidth() / 14,
                Gdx.graphics.getWidth() / 7, Gdx.graphics.getWidth() / 7);
        batch.draw(cSprite, cells.get(2).getBody().getPosition().x - 10 - Gdx.graphics.getWidth() / 14,
                cells.get(2).getBody().getPosition().y + 10 - Gdx.graphics.getWidth() / 14,
                Gdx.graphics.getWidth() / 7, Gdx.graphics.getWidth() / 7);
        shadow.draw(batch, "|Time| " + pref.getTime() + " sec", Gdx.graphics.getWidth() / 18, Gdx.graphics.getWidth()/18);
        font.draw(batch, "|Time| " + pref.getTime() + " sec", Gdx.graphics.getWidth() / 18 - 2, Gdx.graphics.getWidth()/18 + 2);
        shadow.draw(batch, "|Pressure| " + pref.getMenuScore(), Gdx.graphics.getWidth() / 18, Gdx.graphics.getWidth()/36);
        font.draw(batch, "|Pressure| " + pref.getMenuScore(), Gdx.graphics.getWidth() / 18 - 2, Gdx.graphics.getWidth()/36 + 2);
        batch.end();
    }

    private void setCol(int cl, Cell c) {
        switch (cl) {
            case 0:
                shapeRenderer.setColor(110 / 255f, 40 / 255f, 40 / 255f, 0);
                break;
            case 1:
                shapeRenderer.setColor(110 / 255f, 110 / 255f, 40 / 255f, 0);
                break;
            case 2:
                shapeRenderer.setColor(40 / 255f, 110 / 255f, 40 / 255f, 0);
                break;
            case 3:
                shapeRenderer.setColor(40 / 255f, 110 / 255f, 110 / 255f, 0);
                break;
            case 4:
                shapeRenderer.setColor(40 / 255f, 40 / 255f, 110 / 255f, 0);
                break;
            case 5:
                shapeRenderer.setColor(110 / 255f, 40 / 255f, 110 / 255f, 0);
                break;
            default:
                shapeRenderer.setColor(110 / 255f, 110 / 255f, 110 / 255f, 0);
                break;
        }
    }
}