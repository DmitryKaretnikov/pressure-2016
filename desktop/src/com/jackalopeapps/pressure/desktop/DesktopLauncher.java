package com.jackalopeapps.pressure.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jackalopeapps.pressure.PressureGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Pressure";
		config.width = 396;
		config.height = 704;
		new LwjglApplication(new PressureGame(), config);
	}
}